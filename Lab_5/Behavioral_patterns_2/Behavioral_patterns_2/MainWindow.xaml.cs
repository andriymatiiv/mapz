﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Behavioral_patterns_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RestoreCommand = new RestoreHealthCommand(server);
        }
        Client user1;
        Client user2;
        Server server = new Server();
        ICommand RestoreCommand;
        void Update_users(int user)
        {
            Health_1.Value = user1.HealthPoints;
            Health_2.Value = user2.HealthPoints;
            if (user == 1)
            {
                Output_2.Items.Add("I was Attacked");
            }
            else if(user == 2)
            {
                Output_1.Items.Add("I was Attacked");
            }
            else
            {
                
            }
            if (user1.HealthPoints <= 0 || user2.HealthPoints <= 0)
            {
                if(user1.HealthPoints <= 0)
                {
                    MessageBox.Show($"{user2.username} won!");
                }
                else
                {
                    MessageBox.Show($"{user1.username} won! ");
                }
                RestoreCommand.Execute(new Data());
                Update_users(0);
            }
        }
        bool is1Connected = false;
        bool is2Connected = false;
        private void Connect_1_Click(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(Connect_1))
            {
                if (!is1Connected)
                {
                    user1 = new Client(Username_1.Text, server);
                    server.Connect(ref user1);
                    is1Connected = true;
                    Connect_1.Content = "Disconnect";
                }
                else
                {
                    user1 = null;
                    Data data = new Data();
                    data.int_data_param = user1.id;
                    server.Disconnect(data);
                    is1Connected = false;
                    Connect_1.Content = "Connect";
                }
            }
            else
            {
                if (!is2Connected)
                {
                    user2 = new Client(Username_2.Text, server);
                    server.Connect(ref user2);
                    is2Connected = true;
                    Connect_2.Content = "Disconnect";
                }
                else
                {
                    user2 = null;
                    Data data = new Data();
                    data.int_data_param = user2.id;
                    server.Disconnect(data);
                    is2Connected = false;
                    Connect_2.Content = "Connect";
                }
            }
        }

        private void Attack_1_Click(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(Attack_1))
            {
                user1.Attack(user2.id);
                Update_users(1);
            }
            else
            {
                user2.Attack(user1.id);
                Update_users(2);
            }
        }
    }
}
