﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace Behavioral_patterns_2
{
    class Client 
    {
        public string username { get; private set; }
        public int HealthPoints { get; private set; }
        ICommand AttackCommand;
        public Client (string username, Server server)
        {
            this.username = username;
            HealthPoints = 100;
            AttackCommand = new AttackCommand(server);
        }
        public int id { get; set; }
        public int Defend()
        {
            Random rnd = new Random();
            int Res = rnd.Next(1, 5);
            if (Res != 1)
            {
                if (HealthPoints >= 10)
                {
                       HealthPoints -= 10;
                }   
            }
            return HealthPoints;
        }
        public void Attack(int us_id)
        {
            Data data;
            data.int_data_param = us_id;
            data.string_data_param = "Attack";
            AttackCommand.Execute(data);
        }
        public void RestoreHealth()
        {
            HealthPoints = 100;
        }
    }

    class Server
    {
        List<Client> playsers = new List<Client>();
        int nextid = 0;
        public bool Connect(ref Client client)
        {
            client.id = nextid;
            playsers.Add(client);
            nextid++;
            return true;
        }
        public void  Disconnect(Data data)
        {
            Client client = playsers.Find(item => item.id == data.int_data_param);
            if (client != null)
            {
                playsers.Remove(client);
            }
            return;
        }
        public void Attack(Data data)
        {
            Client client = playsers.Find(item => item.id == data.int_data_param);
            client.Defend();
        }
        public void RestoreHealth(Data data)
        {
            foreach(var item in playsers)
            {
                item.RestoreHealth();
            }
        }
    }


    abstract class ICommand 
    {
        protected Server server;
        public ICommand(Server server)
        {
            this.server = server;
        }
       abstract public void  Execute(Data data);
    }
    class RestoreHealthCommand :ICommand
    {
        public RestoreHealthCommand(Server server) : base(server)
        {

        }
        public override void Execute(Data data)
        {
            server.RestoreHealth(data);
        }
    }
    class AttackCommand : ICommand
    {
        public AttackCommand(Server server) : base(server)
        {

        }
        public override void Execute(Data data)
        {
            server.Attack(data);
        }
    }
    struct Data
    {
        public string string_data_param;
        public int int_data_param;
    }
}
