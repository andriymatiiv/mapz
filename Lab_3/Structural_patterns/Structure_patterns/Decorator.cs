﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media.Animation;

namespace Structure_patterns
{
    abstract class Player 
    {
        public string username { get; protected set; }
        public Player(string name)
        {
            username = name;
        }
        public abstract string ShowProperties();
        protected int startmoney;
        public double resourcesIndex;
    }
    class StandartPlayer :Player
    {
        public StandartPlayer(string name) : base(name)
        {
            startmoney = 10000;
            resourcesIndex = 1.0;
        }
        public override string ShowProperties()
        {
            return $"Name: {username }, StartMoney: {startmoney}, Resources index: {resourcesIndex}";
        }
    }
    class PremiumPlayer : Player
    {
        public PremiumPlayer(string name) : base(name)
        {
            startmoney = 30000;
            resourcesIndex = 1.4;
        }
        public override string ShowProperties()
        {
            return $"Name: {username }, StartMoney: {startmoney}, Resources index: {resourcesIndex}";
        }
    }
    abstract class PlayerDecorator :Player
    {
        public Player player;
        protected PlayerDecorator(Player item) :base (item.username)
        {
            player = item;
        }
        //public string ShowInfo()
        //{
        //    return player.ShowProperties();
        //}
    }
    class PowerDecorator : PlayerDecorator 
    {
        public PowerDecorator(Player player) : base(player) 
        {
        }
        public override string ShowProperties()
        {
            return player.ShowProperties() + ", Power boost: 1.5";
        }
    }
    class ResourcesDecorator : PlayerDecorator
    {
        public ResourcesDecorator(Player player) : base(player)
        {
        }
        public override string ShowProperties()
        {
            return player.ShowProperties() + ", SpeedBoost: 1.1";
        }
    }

}
