﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Structure_patterns
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Facade_Click(object sender, RoutedEventArgs e)
        {
            int money = Int32.Parse(MoneyCount.Text);
            Output.Clear();
            Baker baker = new Baker();
            int Bread = 0;
            Output.Text += baker.BakeBread(money, ref Bread);
        }
        List<Player> Players = new List<Player>();
        Player usualplayer;
        Player premiumplayer;
        int current = 0;
        private void User_Click(object sender, RoutedEventArgs e)
        { 
            Player player = new PremiumPlayer("Premium");
            Players.Add(player);
            //player = new StandartPlayer("Usual");
            //Players.Add(player);
            if (sender.Equals(UsualUser))
            {
                usualplayer = new StandartPlayer("Usual");
                Players.Add(player);
                Output.Text += usualplayer.ShowProperties() + "\n";
                current = 1;
            }else if (sender.Equals(PremiumUser))
            {
                premiumplayer = new PremiumPlayer("Premium");
                Output.Text += Players[0].ShowProperties() + "\n";
                current = 0;
            }
        }


        private void Power_Click(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(Power))
            {
                if (current == 0 )
                {
                    premiumplayer = new PowerDecorator(premiumplayer);
                    Output.Text += premiumplayer.ShowProperties() +"\n";
                }
                else
                {
                    usualplayer = new PowerDecorator(usualplayer);
                    Output.Text += usualplayer.ShowProperties() + "\n";
                }
                
            }else if (sender.Equals(Resources))
            {
                if (current == 0)
                {
                    premiumplayer = new ResourcesDecorator(premiumplayer);
                    Output.Text += premiumplayer.ShowProperties() + "\n";
                }
                else
                {
                    usualplayer = new ResourcesDecorator(usualplayer);
                    Output.Text += usualplayer.ShowProperties() + "\n";
                }
            }
        }
        Flyweight factory = new Flyweight();
        private void Draw_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            if (sender.Equals(MillDraw))
            {
                Output.Text += factory.GetItem("Mill").Draw(rnd.Next(0, 1000), rnd.Next(0,1000));
            }else if (sender.Equals(CastleDraw))
            {
                Output.Text += factory.GetItem("Castle").Draw(rnd.Next(0, 1000), rnd.Next(0, 1000));
            }
            else if (sender.Equals(StorageDraw))
            {
                Output.Text += factory.GetItem("Storage").Draw(rnd.Next(0, 1000), rnd.Next(0, 1000));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Output.Clear();
        }
    }
}
