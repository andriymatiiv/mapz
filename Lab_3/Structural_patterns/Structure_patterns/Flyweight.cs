﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Structure_patterns
{
    class Flyweight
    {
        Dictionary<string, IBuilding> list = new Dictionary<string, IBuilding>();
        public IBuilding GetItem(string key)
        {
            IBuilding item = null;
            if (list.ContainsKey(key))
            {
                item = list[key];
            }else
            { 
                switch (key)
                {
                    case "Castle": 
                        {
                            item = new Castle_ui();
                            list.Add(key, item);
                            break;
                        }
                    case "Storage":
                        {
                            item = new Storage_ui();
                            list.Add(key, item);
                            break;

                        }
                    case "Mill":
                        {
                            item = new Mill_ui();
                            list.Add(key, item);
                            break;
                        }
                    default:
                        break;
                }
            }
            return item;
        }
    }
    abstract class IBuilding {
        protected int width;
        protected int lenght;
        protected int id = 0;
        public abstract string Draw(int x, int y);
    }

    class Mill_ui : IBuilding
    {
        int height = 5;
        double radius = 0.8;
        public Mill_ui()
        {
            width = 2;
            lenght = 2;
            ++id;
        }
        public override string Draw(int x, int y)
        {
            return $"Mill was drawed at [{x},{y}]. its` width: {width}, its lenght: {lenght}.Height {height}. Basis radius: {radius}. id = {id}\n" ;
        }
    }
    class Castle_ui : IBuilding
    {
        int topHeight = 15;
        //int lowHeight = 10;
        //string 
        public Castle_ui()
        {
            width = 5;
            lenght = 5;
            ++id;
        }
        public override string Draw(int x, int y)
        {
            return $"Castle was drawed at [{x},{y}]. its` width: {width}, its lenght: {lenght}. Top height {topHeight}. id = {id}\n";
        }
        
    }
    class Storage_ui : IBuilding
    {
        string form = "small house-like building";
        public Storage_ui()
        {
            width = 2;
            lenght = 3;
            ++id;
        }
        public override string Draw(int x, int y)
        {
            return $"Castle was drawed at [{x},{y}]. its` width: {width}, its lenght: {lenght}. it is {form}. id = {id} \n";
        }
     }
}
