﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structure_patterns
{
    class Baker
    {
        Mill mill = new Mill();
        Castle castle = new Castle();
        Bakery bakery = new Bakery();
        Field field = new Field();
        Storage storage = new Storage();
       public string BakeBread(int money, ref int Breads)
        {
            string res = "";
            int Flour = 0;
            int Wheat = 0;
            int Bread = 0;
            res += castle.GetMoneys(money) + "\n";
            res += field.GrowWheat(money, ref Wheat) + "\n";
            res += mill.ProductFlour(Wheat, ref Flour) + "\n";
            res += bakery.BakeBread(Flour, ref Bread) + "\n";
            res += storage.PutGoods(Bread);
            return res;
        }
    }

    //замок
    class Castle
    {
        public string GetMoneys(int count)
        {
            return $"{count} coins was given";
        }
    }
    //cклад
    class Storage
    {
        public int GoodsCount { get; private set; }
        public string PutGoods(int count)
        {
            return $"{count} breads were put to storage";
        }
        public string GetGoods(int count)
        {
            string res = "";
            if(GoodsCount >= count)
            {
                GoodsCount -= count;
                res = $"{count} breeads taken";
            }
            else
            {
                res = "Not enought breads in storage";
            }
            return res;
        }
    }
    // млин
    class Mill
    {
        public string ProductFlour (int Wheat, ref int ProductedFlour)
        {
            int Flour = Convert.ToInt32(Wheat * 0.8);
            ProductedFlour = Flour;
            string res = $"{Flour} kilo of Flour Producted";
            return res;
        }
    }

    //поле
    class Field
    {
        public string GrowWheat(int money, ref int Wheat)
        {
            Wheat = Convert.ToInt32(money / 4);
            return $"{Wheat} kilo of wheat was grown";
        }
    }

    //пекарня
    class Bakery
    {
        public string BakeBread(int Flour, ref int Bread)
        {
            int Breads = Flour / 2;
            Bread = Breads;
            return $"{Breads } Breads baked";
        }
    }
}
