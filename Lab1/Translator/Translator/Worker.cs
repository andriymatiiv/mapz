﻿using System;
using System.Collections.Generic;
using System.Text;
using Parser;
using System.Windows;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Controls;

namespace Translator
{
    class Worker
    {
       System.Windows.Controls.TextBox box;
       public Worker(in System.Windows.Controls.TextBox Elem) => box = Elem;
       public Worker() { }
       public  bool RunTree(Queue<TreeItem> PolishTree, in List<Variable> VariablesList, in TreeViewItem Root)
        {
            List<Variable> Params = new List<Variable>();
            TreeViewItem InnerRoot = new TreeViewItem();
            InnerRoot.IsExpanded = true;
            Root.Items.Add(InnerRoot);
            
            bool RunBlock = false;
            while (PolishTree.Count != 0)
            {
                TreeItem Item = PolishTree.Dequeue();
                if (Item.item.GetType() == typeof(Variable))
                {
                    int index = VariablesList.FindIndex(Var => Var.Name == ((Variable)Item.item).Name);
                    if (index >= 0)
                    {
                        ((Variable)Item.item).Value = VariablesList[index].Value;
                    }
                    Params.Add((Variable)Item.item);
                }
                else if (Item.item.GetType() == typeof(VarValue))
                {
                    Params.Add(new Variable(null, null, ((VarValue)Item.item).GetValue()));
                }
                else if (Item.item.GetType() == typeof(Equation))
                {
                    InnerRoot.Header = "=";
                    Params[0].Value = Params[1].Value;
                    int Index = VariablesList.FindIndex(Var => Var.Name == Params[0].Name);
                    VariablesList[Index].Value = Params[0].Value;
                    InnerRoot.Items.Add(new TreeViewItem() { Header = VariablesList[Index].Name });
                    InnerRoot.Items.Add(new TreeViewItem() { Header = Params[1].Value });
                    while (Params.Count > 0)
                    {
                        Params.RemoveAt(0);
                    }
                }
                else if (Item.item.GetType() == typeof(MathOperator))
                {
                    string Operator = ((MathOperator)Item.item).type;
                    //int val1 = int.Parse(Params[Params.Count - 2].Value);
                    int val1 = int.Parse(Params[Params.Count - 2].Value);
                    int val2 = int.Parse(Params[Params.Count - 1].Value);
                    
                    TreeViewItem item = new TreeViewItem() { Header = Operator };
                    item.IsExpanded = true;
                    item.Items.Add(new TreeViewItem() { Header = val1 });
                    item.Items.Add(new TreeViewItem() { Header = val2 });
                    InnerRoot.Items.Add(item);
                    switch (Operator)
                    {
                        case "*":
                            {
                                Params[Params.Count - 2].Value = (val1 * val2).ToString();
                                Params.RemoveAt(Params.Count - 1);
                            }
                            break;
                        case "/":
                            {
                                Params[Params.Count - 2].Value = (val1 / val2).ToString();
                                Params.RemoveAt(Params.Count - 1);
                            }
                            break;
                        case "+":
                            {
                                Params[Params.Count - 2].Value = (val1 + val2).ToString();
                                Params.RemoveAt(Params.Count - 1);
                            }
                            break;
                        case "-":
                            {
                                Params[Params.Count - 2].Value = (val1 - val2).ToString();
                                Params.RemoveAt(Params.Count - 1);
                            }
                            break;
                        default:
                            break;
                    }

                }
                else if (Item.item.GetType() == typeof(Function))
                {
                    string Name = ((Function)Item.item).Name;
                    InnerRoot.Header = Name;
                    if (Name == "Print")
                    {
                        int Index = VariablesList.FindIndex(Var => Var.Name == Params[0].Name);
                        try
                        {
                            if (Index < 0)
                            {
                                Print(Params[Params.Count - 1].Value);
                                InnerRoot.Items.Add(new TreeViewItem() { Header = Params[Params.Count - 1].Value });
                            }
                            else
                            {
                                Print(VariablesList[Index].Value);
                                InnerRoot.Items.Add(new TreeViewItem() { Header = VariablesList[Index].Value });
                            }
                        }
                        catch (Exception ex)
                        {
                            box.Text = ex.Message;
                        }
                    }
                    else if (Name == "Run")
                    {
                        int Index = VariablesList.FindIndex(Var => Var.Name == Params[0].Name);
                        try
                        {
                            if (Index < 0)
                            {
                                Run(Params[Params.Count - 1].Value);
                                InnerRoot.Items.Add(new TreeViewItem() { Header = Params[Params.Count - 1].Value });
                            }
                            else
                            {
                                Run(VariablesList[Index].Value);
                                InnerRoot.Items.Add(new TreeViewItem() { Header = VariablesList[Index].Value });
                            }
                        }
                        catch (Exception ex)
                        {
                            box.Text = ex.Message;
                        }
                    }
                    else if (Name == "MoveCursor")
                    {
                        InnerRoot.Items.Add(new TreeViewItem() { Header = Params[Params.Count - 2].Value, IsExpanded = true });
                        InnerRoot.Items.Add(new TreeViewItem() { Header = Params[Params.Count - 1].Value, IsExpanded = true });
                        NativeMethods.SetCursorPos(int.Parse(Params[Params.Count - 2].Value), int.Parse(Params[Params.Count - 1].Value));
                    }
                }
                else if (Item.item.GetType() == typeof(ConditionalStatement))
                {
                    string CondName = ((ConditionalStatement)Item.item).name;

                    InnerRoot.Header = CondName;
                    InnerRoot.Items.Add(Params[Params.Count - 2].Value);
                    InnerRoot.Items.Add(Params[Params.Count - 1].Value);

                    int val1 = int.Parse(Params[Params.Count - 2].Value);
                    int val2 = int.Parse(Params[Params.Count - 1].Value);
                    switch (CondName)
                    {
                        case "==":
                            {
                                if (val1 == val2)
                                {
                                    RunBlock = true;
                                }
                                break;
                            }
                        case ">":
                            {
                                if (val1 > val2)
                                {
                                    RunBlock = true;
                                }
                                break;
                            }
                        case "<":
                            {
                                if (val1 < val2)
                                {
                                    RunBlock = true;
                                }
                                break;
                            }

                        case "!=":
                            {
                                if (val1 != val2)
                                {
                                    RunBlock = true;
                                }
                                break;
                            }
                        case ">=":
                            {
                                if (val1 >= val2)
                                {
                                    RunBlock = true;
                                }
                                break;
                            }
                        case "<=":
                            {
                                if (val1 <= val2)
                                {
                                    RunBlock = true;
                                }
                                break;
                            }
                        default:
                            RunBlock = false;
                            break;
                    }
                }
            }
            return RunBlock;
        }
       
        void Print(string data) => box.Text += data + "\n";
        void Run(string filename)
        {
            try
            {
                Process.Start(filename);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        public partial class NativeMethods
        {
            [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "SetCursorPos")]
            [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
            public static extern bool SetCursorPos(int X, int Y);
        }
    }
}
