﻿using Lekser;
using System;
using System.Collections.Generic;
using System.Linq;
using Translator;
using System.Windows.Controls;
namespace Parser
{
    class Parcer
    {
        List<Variable> VariablesList = new List<Variable>();
        TextBox box;
        TreeViewItem ParcerTree;
        public Parcer(out List<Variable> VarList, in System.Windows.Controls.TextBox Elem, in TreeViewItem Tree )
        {
            box = Elem;
            VarList = VariablesList;
            ParcerTree = Tree;
        }

        public Queue<TreeItem> Parce(List<Token> TokensList, ref int Pos, ref bool End)
        {
            Queue<TreeItem> PolishTree = new Queue<TreeItem>();
            Stack<Operators> OperatorsList = new Stack<Operators>();
            string type = null;
            bool doElse = false;

            for (; Pos < TokensList.Count();++Pos)
            {
                Token token = TokensList[Pos];
                
                if(token.type == TokenType.Keyword )
                {
                    if (token.value == "var")
                    {
                        type = token.value;
                    }
                    else if (token.value == "string")
                    {
                        type = token.value;
                    }
                    else if (token.value == "if" || token.value == "while")
                    {
                        ++Pos;
                        List<Token> Statement = new List<Token>();
                        while (TokensList[++Pos].value != ")")
                        {
                            Statement.Add(TokensList[Pos]);
                        }
                        Pos += 1;

                        List<Token> Block = new List<Token>();
                        while (TokensList[Pos].value != "}")
                        {
                            Block.Add(TokensList[Pos++]);
                        }
                        //++Pos;
                        if (token.value == "if")
                        {
                            Worker item = new Worker(box);
                            int pozition = 0;
                            bool end = false;
                            TreeViewItem node = new TreeViewItem() { Header = "if" };
                            node.IsExpanded = true;
                            ParcerTree.Items.Add(node);
                            Queue<TreeItem> StatementTree = Parce(Statement, ref pozition, ref end);
                            bool run = item.RunTree(StatementTree, VariablesList,node);
                            if(run)
                            {
                                doElse = false;
                            }
                            else
                            {
                                doElse = true;
                            }
                            pozition = 0;
                            if (run)
                            {
                                do
                                {
                                    Queue<TreeItem> BlockTree = Parce(Block, ref pozition, ref end);
                                    item.RunTree(BlockTree, VariablesList, node);
                                } while (!end);
                            }
                        }
                        else if (token.value == "while")
                        {
                            Worker item = new Worker(box);
                            int pozition = 0;
                            bool end = false;
                            TreeViewItem node = new TreeViewItem() { Header = "while" };
                            node.IsExpanded = true;
                            ParcerTree.Items.Add(node);
                            Queue<TreeItem> StatementTree = Parce(Statement, ref pozition, ref end);
                            bool run = item.RunTree(StatementTree, VariablesList, node);
                            
                            pozition = 0;
                            while (run)
                            {
                                do
                                {
                                    Queue<TreeItem> BlockTree = Parce(Block, ref pozition, ref end);
                                    item.RunTree(BlockTree, VariablesList, node);
                                } while (!end);

                                pozition = 0;
                                StatementTree = Parce(Statement, ref pozition, ref end);
                                pozition = 0;
                                run = item.RunTree(StatementTree, VariablesList, node);
                                pozition = 0;
                            }
                        }
                    }
                    else if (token.value == "else")
                    {
                        ++Pos;
                        List<Token> Block = new List<Token>();
                        while (TokensList[Pos].value != "}")
                        {
                            Block.Add(TokensList[Pos++]);
                        }
                        if (doElse)
                        {
                            TreeViewItem node = new TreeViewItem() { Header = "Else" };
                            node.IsExpanded = true;
                            ParcerTree.Items.Add(node);
                            bool end = false;
                            Worker item = new Worker(box);
                            int pozition = 0;
                            do
                            {
                                Queue<TreeItem> BlockTree = Parce(Block, ref pozition, ref end);
                                item.RunTree(BlockTree, VariablesList, node);
                            }while(!end);
                        }

                    }
                }
                else if(token.type == TokenType.Variable)
                {
                    if (!VariablesList.Any(Var => Var.Name == token.value))
                    {
                        VariablesList.Add(new Variable(type, token.value, null));
                    }

                    PolishTree.Enqueue(new TreeItem(new Variable(type, token.value, null)));
                }
                else if (token.type == TokenType.Equal)
                {
                    if (OperatorsList.Any())
                    {
                        while(OperatorsList.Peek().Prioritet >= Priority.Egual)
                        {
                            PolishTree.Enqueue(new TreeItem(OperatorsList.Pop().Operator));
                        }
                    }
                    OperatorsList.Push(new Operators(new Equation(), Priority.Egual));
                }
                else if (token.type == TokenType.Value)
                {
                    PolishTree.Enqueue(new TreeItem(new VarValue(token.value)));
                }
                else if (token.type == TokenType.Brackets)
                {
                    if(token.value == "(")
                    {
                        OperatorsList.Push(new Operators(new LeftBracket(), 0));
                    }
                    else if (token.value == ")")
                    {
                        if (OperatorsList.Any())
                        {
                            while (OperatorsList.Peek().Operator.GetType() != typeof(LeftBracket))
                            {
                                PolishTree.Enqueue(new TreeItem(OperatorsList.Pop().Operator));
                            }
                            OperatorsList.Pop();
                        }
                    }
                    else if (token.value == "{")
                    {

                    }
                    else if (token.value == "}")
                    {

                    }
                }
                else if (token.type == TokenType.Operator)
                {
                    if (token.value == "/" || token.value =="*")
                    {
                        if (OperatorsList.Any())
                        {
                            while(OperatorsList.Peek().Prioritet >= Priority.MulDiv)
                            {
                                PolishTree.Enqueue(new TreeItem(OperatorsList.Pop().Operator));
                            }
                        }
                        OperatorsList.Push(new Operators(new MathOperator(token.value), Priority.MulDiv));
                    }
                    else
                    {
                        if (OperatorsList.Any()) { 
                            while (OperatorsList.Peek().Prioritet >= Priority.SumSub)
                            {
                                PolishTree.Enqueue(new TreeItem(OperatorsList.Pop().Operator));
                            }
                        }
                        OperatorsList.Push(new Operators(new MathOperator(token.value), Priority.SumSub));
                    }
                }
                else if (token.type ==TokenType.Semicolon)
                {
                    ++Pos;
                    break;
                }
                else if (token.type == TokenType.Function)
                {
                    if (OperatorsList.Any())
                    {
                        while (OperatorsList.Peek().Prioritet >= Priority.Func)
                        {
                            PolishTree.Enqueue(new TreeItem(OperatorsList.Pop().Operator));
                        }
                    }
                    OperatorsList.Push(new Operators(new Function(token.value), Priority.Func));
                }
                else if (token.type == TokenType.Compare)
                {
                    OperatorsList.Push(new Operators(new ConditionalStatement(token.value), Priority.Compare));
                }
            }
            while (OperatorsList.Any())
            {
                PolishTree.Enqueue(new TreeItem(OperatorsList.Pop().Operator));
            }
            if (Pos >= TokensList.Count())
            {
                End = true;
            }
            else
            {
                End = false;
            }
            return PolishTree;
        }
    }
    enum Priority
    {
        Egual = 1,
        MulDiv = 3,
        SumSub = 2,
        Power = 4,
        Func = 5,
        Compare = 3,
    }
    class Operators
    {
        public object Operator;
        public Priority Prioritet;
        public Operators(object item, Priority prior)
        {
            Operator = item; Prioritet = prior;
        }
    }
    class TreeItem
    {
         public object item;
        public TreeItem(object Item)
        {
            item = Item;
        }
    }
    class Function
    {
        public string Name;
         public Function(string name) => Name = name; 
    }
    class Variable
    {
         public string Type, Name, Value;
        public Variable (string type, string name, string value)
        {
            Type = type;
            Name = name;
            Value = value;
        }
        public Variable(string name)
        {
            Name = name;
        }
        public string  GetName()
        {
            return this.Value;
        }
    }
    class LeftBracket
    {
        
    }
    class RightBracket
    {

    }
    class LeftBigBracket
    {

    }
    class RightBigBracket
    {

    }
    class WhileStatement
    { 
    
    }
    class ConditionalStatement
    {
        public string name;
        public ConditionalStatement(string condName)
        {
            if (string.IsNullOrWhiteSpace(condName))
            {
                throw new ArgumentException("message", nameof(condName));
            }

            name = condName;
        }
    }
    class Equation
    {

    }
    class VarValue
    {
        string value;
        public VarValue(string val) => value = val;
        public string GetValue()
        {
            return value;
        }
    }
    class MathOperator
    {
        public string type;
        public MathOperator(string Type) => type = Type;
    }
}