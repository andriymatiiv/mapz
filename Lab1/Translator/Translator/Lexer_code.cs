﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Lekser
{
    enum TokenType
    {
        Equal,
        Variable,
        Value,
        Cycle,
        Compare,
        Symbols,
        Brackets,
        Keyword,
        Operator, 
        Semicolon,
        Function,
    }
    class Token
    {
        public Token(TokenType Type, string Value) { type = Type; value = Value;}
        public TokenType type;
        public string value;
    }
    class LexAnalysis
    {
        private string code;
        List<string> KeyWords = new List<string>() { "if", "else", "while",  "var", "string" };
        List<string> Functions = new List<string>() { "Click", "Run", "Press", "DragAndDrop", "Print", "MoveCursor"};
        List<string> Operators = new List<string>() { "+", "-", "/", "*"};
        List<string> Brackets = new List<string>() { "(", ")", "{", "}"};
        List<string> Comparison = new List<string>() { "==",">","<",">=","<=","!=" };
        public LexAnalysis(string Icode)
        {
            code = Icode;
        }
        public List<Token> CreateTokens()
        {
            List<Token> Tokens = new List<Token>();

            string[] sLines = code.Split('\n', StringSplitOptions.RemoveEmptyEntries);
            for(int i=0; i< sLines.Length; ++i)
            {
                string Line = sLines[i];
                if (Line.Contains("//"))
                {
                    Line = Line.Remove(Line.IndexOf("//"), Line.Length - Line.IndexOf("//"));
                }
                char[] delimeters = new char[] {'\t', ' ', '\r', ','};
                string[] words = Line.Split(delimeters, StringSplitOptions.RemoveEmptyEntries);
                foreach (string text_node in words)
                {
                    if (text_node == "=")
                    {
                        Tokens.Add(new Token(TokenType.Equal, text_node));
                    }
                    else if (Comparison.Contains(text_node))
                    {
                        Tokens.Add(new Token(TokenType.Compare, text_node));
                    }
                    else if (Brackets.Contains(text_node))
                    {
                        Tokens.Add(new Token(TokenType.Brackets, text_node));
                    }
                    else if (Operators.Contains(text_node))
                    {
                        Tokens.Add(new Token(TokenType.Operator, text_node));
                    }
                    else if (text_node == ";")
                    {
                        Tokens.Add(new Token(TokenType.Semicolon, text_node));
                    } else if (Functions.Contains(text_node))
                    {
                        Tokens.Add(new Token(TokenType.Function, text_node));
                    }
                    else if (KeyWords.Contains(text_node))
                    {
                        Tokens.Add(new Token(TokenType.Keyword, text_node));
                    }
                    else if (text_node[0] == '\"' && text_node[text_node.Length-1] == '\"')
                    {
                        Tokens.Add(new Token(TokenType.Value, text_node));
                    }
                    else if (text_node[0] == '-')
                    {
                        Tokens.Add(new Token(TokenType.Value, text_node));
                    }
                    else 
                    {
                        Regex rxNums = new Regex(@"^\d+$");
                        if (rxNums.IsMatch(text_node))
                        {
                            Tokens.Add(new Token(TokenType.Value, text_node));
                        }
                        else
                        {
                            Tokens.Add(new Token(TokenType.Variable, text_node));
                        }
                    }
                }
            }
            return Tokens;
        }
    }
}
