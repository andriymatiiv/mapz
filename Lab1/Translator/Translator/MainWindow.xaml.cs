﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using Lekser;
using Parser;
using System.Windows.Input;
using System.Threading;

namespace Translator
{
    public partial class MainWindow : Window
    {
        bool record = false;
        private List<Point> PointsList = new List<Point>();
        private List<Key> KeyList = new List<Key>();

        public MainWindow()
        {
            InitializeComponent();

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OutputText.Text = "";
            Result.Text = "";
            string code = InputText.Text;
            try
            {
                LexAnalysis item = new LexAnalysis(code);
                List<Token> tokens = item.CreateTokens();
                List<Variable> Variables = new List<Variable>();
                foreach (Token elem in tokens)
                {
                    string token =$"{elem.type} : {elem.value} \n";
                    OutputText.Text = OutputText.Text +  token;
                }
                int PercerPos = 0;
                ParcerTree.Items.Clear();
                TreeViewItem Item = new TreeViewItem() { Header = "Root"};
                Item.IsExpanded = true;
                Parcer parser = new Parcer(out Variables, Result, Item);
                bool end = false;
                ParcerTree.Items.Add(Item);
                Worker worker = new Worker(Result);
                do
                {
                    worker.RunTree(parser.Parce(tokens, ref PercerPos, ref end), Variables, Item);
                }
                while (!end);
                }
            catch (Exception ex)
            {
                Result.Text = ex.Message;
            }
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            if(!record)
            {
                record = true;
                PointsList.Clear();
                KeyList.Clear();
                Button2.Content = "Stop";
            }
            else
            {
                record = false;
                Button2.Content = "Record";
            }
        }

        private void ScanItem_MouseMove(object sender, MouseEventArgs e)
        {
            if (record)
            {
                var windowPosition = Mouse.GetPosition(this);
                var screenPosition = this.PointToScreen(windowPosition);
                PointsList.Add(screenPosition);
            }
        }

        private void ScanItem_KeyUp(object sender, KeyEventArgs e)
        {
            if (record)
            {
                //inputtedText += e.Key.ToString();
                KeyList.Add(e.Key);
            }
        }

        private void ScanItem_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        public static void Send(Key key)
        {
            bool res = false;
            if (Keyboard.PrimaryDevice != null)
            {
                if (Keyboard.PrimaryDevice.ActiveSource != null)
                {
                    var e = new KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0, key)
                    {
                        RoutedEvent = Keyboard.KeyDownEvent
                    };
                    //Keyboard.FocusedElement.RaiseEvent(e);
                    res = InputManager.Current.ProcessInput(e);
                }
            }
            if (!res)
            {
                var eventArgs = new TextCompositionEventArgs(Keyboard.PrimaryDevice,
                new TextComposition(InputManager.Current, Keyboard.FocusedElement, key.ToString()));
                eventArgs.RoutedEvent = TextInputEvent;
                InputManager.Current.ProcessInput(eventArgs);
            }

        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            ScanRes.Focus();
            foreach (Point item in PointsList)
            {
                Thread.Sleep(20);
                NativeMethods.SetCursorPos((int)item.X, (int)item.Y);
            }
            foreach (Key item in KeyList)
            {
                Send(item);
            }
        }
        public partial class NativeMethods
        {
            [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "SetCursorPos")]
            [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
            public static extern bool SetCursorPos(int X, int Y);
        }
    }
}
