﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tvirni_Patterns
{    
    // загальний клас юніт
    class Army
    {
        public IWeapon weapon;
        IArmor armor;
        IMovement movement;

        public Army(IArmyFactory factory)
        {
            weapon = factory.CreateWeapon();
            armor = factory.CreateArmor();
            movement = factory.CreateMovement();
        }
        public string Move()
        {
            return movement.Move();
        }
        public string Hit()
        {
             return weapon.Hit();
        }
        public bool RestoreHealth(ref int moneys)
        {
            return armor.RestoreHealth(ref moneys);
        }
    }
    // загальна фаббрика
    interface IArmyFactory
    {
        IMovement CreateMovement();
        IWeapon CreateWeapon();
        IArmor CreateArmor();
    }
    interface IMovement
    {
        string Move();
    }
    interface IWeapon
    {
        string Hit();
        
    }

    interface IArmor
    {
        bool defend(int shotPower);
        bool RestoreHealth(ref int moneys);
    }



    // фабрика для кінноти
    class Knightfactory : IArmyFactory
    {
        public IMovement CreateMovement()
        {
            return new KnightMovement();
        }
        public IWeapon CreateWeapon()
        {
            return new KnightWeapon();
        }
        public IArmor CreateArmor()
        {
            return new KnightArmor();
        }
    }

    // фабрика для кавалерії

    class Cavalryfactory : IArmyFactory
    {
        public IMovement CreateMovement()
        {
            return new CavalryMovement();
        }
        public IWeapon CreateWeapon()
        {
            return new CavalryWeapon();
        }
        public IArmor CreateArmor()
        {
            return new CavalryArmor();
        }
    }

    // класи для лицаря
    class KnightMovement : IMovement
    {
        public string Move()
        {
            return "Kniht is moving";
        }
    }
    class KnightWeapon : IWeapon
    {
        int ShotPower = 8;

        public string Hit()
        {
            return "Knight is hitting";
        }

    }
    class KnightArmor : IArmor
    {
        public int Armor { get { return Armor; } private set { Armor = 80; } }
        int MaxArmor = 150;
        int RestoreValue = 100;
        public bool defend(int shotPower)
        {
           if (Armor > shotPower)
            {
                Armor -= shotPower;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool RestoreHealth(ref int moneys)
        {
            if (moneys > RestoreValue)
            {
                moneys -= RestoreValue;
                Armor = MaxArmor;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //класи для кінноти
    class CavalryMovement : IMovement
    {
        int Speed = 15;

        public string Move()
        {
            return "Cavalry is moving";
        }
    }
    class CavalryWeapon : IWeapon
    {
        int ShotPower = 10;
        
        public string Hit()
        {
                return "Cavalry is hitting";
        }
        
    }
    class CavalryArmor : IArmor
    {
        public int Armor { get { return Armor; } private set { Armor = 200; } }
        int maxArmor = 150;
        int RestorePrice = 200;
        public bool defend(int shotPower)
        {
            if (Armor > shotPower)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public bool RestoreHealth(ref int moneys)
        {
            if (moneys > RestorePrice)
            {
                moneys -= 150;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

