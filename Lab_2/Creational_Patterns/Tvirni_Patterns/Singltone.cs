﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tvirni_Patterns
{
    class Server
    {
        private static readonly Server instance = new Server();
        public static Server Instance { get { return instance; } }
        string ServerName = "Secure Server ";
        static int count = 0;

        static Server()
        {
            ++count;
        }
        private Server()
        {
            ++count;
        }
        public string Print()
        {
            return ServerName;
        }
        public void Change(string str)
        {
            ServerName = str;
        }
        public int Getcount()
        {
            return count;
        }
    }
    class Server_NotThreadSecure
    {
        private static Server_NotThreadSecure instance;
        static int Count = 0 ;
        public static Server_NotThreadSecure GetInstance(string name)
        {
            if (instance == null)
            {
                instance = new Server_NotThreadSecure(name);
            }
            return instance;
        }
        string ServerName;
        static Server_NotThreadSecure()
        {

        }
        private Server_NotThreadSecure(string name)
        {
            ServerName = name;
            ++Count;
        }
        public string Print()
        {
            return "Name: " + ServerName + "\n";
        }
        public void Change(string str)
        {
            ServerName = str;
        }
        public int GetCount()
        {
            return Count;
        }
    }
}
