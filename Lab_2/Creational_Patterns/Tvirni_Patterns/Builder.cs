﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tvirni_Patterns
{
    class Creator
    {
        IBuilder Builder;
        public Creator(IBuilder builder)
        {
            this.Builder = builder;
        }
        
        public string Construct()
        {
            string res = "";
            res += Builder.SetMovement();
            res += Builder.SetArmor();
            res += Builder.SetWeapon();
            return res;
        }
    }
    class Unit
    {
        public IWeapon weapon { get; set; }
        public IArmor armor { get; set; }
        public IMovement movement { get; set; }

        public string Move()
        {
            return movement.Move();
        }
        public string Hit()
        {
            return weapon.Hit();
        }
    }
    interface IBuilder
    {
        string SetWeapon();
        string SetArmor();
        string SetMovement();
        Unit GetResult();
    }

    class KnightBuilder : IBuilder
    {
        Unit unit = new Unit();
        public string SetWeapon()
        {
            unit.weapon = new KnightWeapon();
            return "Weapon added successfully\n";
        }
        public string SetArmor()
        {
            unit.armor = new KnightArmor();
            return "Armor added successfully\n";
        }
        public string SetMovement()
        {
            unit.movement = new KnightMovement();
            return "Movement added successfully\n";
        }
        public Unit GetResult()
        {
            return unit;
        }
    }
    class CavalryBuilder : IBuilder
    {
        Unit unit = new Unit();
        public string SetWeapon()
        {
            unit.weapon = new CavalryWeapon();
            return "Weapon added successfully\n";
        }
        public string SetArmor()
        {
            unit.armor = new CavalryArmor();
            return "Armor added successfully\n";
        }
        public string SetMovement()
        {
            unit.movement = new CavalryMovement();
            return "Movement added successfully\n";
        }
        public Unit GetResult()
        {
            return unit;
        }
    }
}
