﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Tvirni_Patterns
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CreateKnight_Click(object sender, RoutedEventArgs e)
        {
            Output.Clear();
            Army unit = new Army(new Knightfactory());
            Output.Text = Output.Text + unit.Move() + "\n";
            Output.Text = Output.Text + unit.Hit() + "\n";
        }

        private void CreateCavalry_Click(object sender, RoutedEventArgs e)
        {
            Output.Clear();
            Army unit = new Army(new Cavalryfactory());
            Output.Text = Output.Text + unit.Move() + "\n";
            Output.Text = Output.Text + unit.Hit() + "\n";
        }

        private void CreateInstance_Click(object sender, RoutedEventArgs e)
        {
            string res = "";
            Output.Clear();
            (new Thread(() =>
            {
                Server server = Server.Instance;
                //Thread.Sleep(10);
                res = server.Print() + server.Getcount() + "\n";

            })).Start();
            Output.Text += res;
            Server server2 = Server.Instance;
            Thread.Sleep(25);
            Output.Text += server2.Print() + server2.Getcount() + "\n";

        }

        private void CreateObject_Click(object sender, RoutedEventArgs e)
        {
            Output.Clear();
            IBuilder builder = new KnightBuilder();
            Creator creator = new Creator(builder);
            Output.Text += creator.Construct();
            Unit unit = builder.GetResult();
            Output.Text += unit.Move() + "\n";
            Output.Text += unit.Hit() + "\n";
        }

        private void CreateInstance_Click_1(object sender, RoutedEventArgs e)
        {
            string res = "";
            Output.Clear();
            (new Thread(() =>
            {
                Server_NotThreadSecure server = Server_NotThreadSecure.GetInstance(" Server1");
                string res = server.Print() + server.GetCount() + "\n";
            })).Start();
            Server_NotThreadSecure server2 = Server_NotThreadSecure.GetInstance(" Server2");
            if(server2 != null)
                Output.Text += server2.Print() + server2.GetCount() + "\n";

        }
    }
}
