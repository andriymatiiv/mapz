﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Behavioral_patterns
{
    [Serializable]
    class Memento
    {
        public int xPos { get; private set; }
        public int yPos { get; private set; }
        public Memento(int x, int y)
        {
            xPos = x;
            yPos = y;
        }
        
    }
    class Caretaker
    {
       string filename = "data.bin";
       public bool SaveMemento(Memento memento)
       {
            bool ok = false;
            try
            {
                WriteToBinaryFile(filename, memento, true);
                ok = true;
            }
            catch (Exception)
            {
                ok = false;
            }
            
           return ok;
       }
        public Memento GetMemento(int id = -1)
        {
            List<Memento> saves = new List<Memento>();
            try
            {
                saves = ReadFromBinaryFile<Memento>(filename);
            }
            catch (Exception)
            {
                throw;
            }
            int count = saves.Count;
            if(id >0 && id < count)
            {
                return saves[id];
            }
            else
            {
                return saves[count-1];
            }
        }
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        public static List<T> ReadFromBinaryFile<T>(string filePath)
        {
            List<T> objects = new List<T>();
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                while(stream.Position != stream.Length)
                {
                    objects.Add((T)binaryFormatter.Deserialize(stream));
                }
            }
            return objects;
        }
    }
}
