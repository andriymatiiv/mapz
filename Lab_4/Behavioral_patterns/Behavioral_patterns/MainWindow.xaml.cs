﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Behavioral_patterns
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Unit unit = null;

        Caretaker history = new Caretaker();

        private void CreateItem_Click(object sender, RoutedEventArgs e)
        {
            unit = new Unit(new UsualMove());
            Output.Text = "Created successfully";
            Move.IsEnabled = true;
        }

        private void Move_Click(object sender, RoutedEventArgs e)
        {
            int x_pos = Convert.ToInt32(x_axys.Text);
            int y_pos = Convert.ToInt32(y_axys.Text);
            
            Output.Text = unit.move(x_pos, y_pos);
            curr_x.Text = unit.currX.ToString();
            curr_y.Text = unit.currY.ToString();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Save_btn_Click(object sender, RoutedEventArgs e)
        {
            history.SaveMemento(unit.SaveState());
        }

        private void Restore_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                unit.RestoreState(history.GetMemento());
            }
            catch (Exception ex)
            {
                Output.Text += ex.Message +"\n";
                return;
            }
            
            curr_x.Text = unit.currX.ToString();
            curr_y.Text = unit.currY.ToString();
        }

        private void Usual_Click(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(Usual))
            {
                unit.movement = new UsualMove();
                Output.Text += "Usual movement set" + "\n";
            }
            else
            {
                unit.movement = new FastMove();
                Output.Text += "fast movement set" + "\n";
            }
        }
    }
}
