﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Behavioral_patterns
{
    class Unit
    {
        public IMovementAlgorithm movement { private get; set; }
        public int currX { get; private set; }
        public int currY { get; private set; }
        public Unit(IMovementAlgorithm movement)
        {
            this.movement = movement;
            currX = 0;
            currY = 0;
        }
        
        public string move(int x, int y)
        {
            List<int> coordinates = new List<int>();
            coordinates.Add(currX);
            coordinates.Add(currY);
            coordinates.Add(x);
            coordinates.Add(y);
            currX = x;
            currY = y;
            return movement.Move(coordinates); ;
        }
        public Memento SaveState()
        {
            return new Memento(currX, currY);
        }
        public void RestoreState(Memento memento)
        {
            currX = memento.xPos;
            currY = memento.yPos;
        }
    }
    interface IMovementAlgorithm
    {
        string Move(List<int> coordinates);
    }
    class UsualMove : IMovementAlgorithm
    {
        public string Move (List<int> coordinates)
        {
            string moving = "";
            int CurrX = coordinates[0];
            int CurrY = coordinates[1];
            int XStep = coordinates[2] - coordinates[0];
            if (XStep < 0) {
                XStep = -1;
            }
            else
            {
                XStep = 1;
            }
            int YStep = coordinates[3] - coordinates[1];
            if (YStep < 0)
            {
                YStep = -1;
            }
            else
            {
                YStep = 1;
            }
            int time = 0;
            while (CurrX != coordinates[2] )
            {
                CurrX += XStep;
                time += 1;
                moving += $"Curr position {CurrX}, {CurrY} time spent {time};  \n";
            }
            while(CurrY != coordinates[3])
            {
                CurrY += YStep;
                time += 1;
                moving += $"Curr position {CurrX}, {CurrY} time spent {time};  \n";
            }

            return moving;
        }
    }
    class FastMove : IMovementAlgorithm
    {
        public string Move(List<int> coordinates) 
        {

            string moving = "";
            double currX = coordinates[0];
            double CurrY = coordinates[1];
            int XStep = coordinates[2] - coordinates[0];
            int YStep = coordinates[3] - coordinates[1];
            int minStep = min(Math.Abs(XStep), Math.Abs(YStep));
            int maxStep = max(Math.Abs(XStep), Math.Abs(YStep));
            int koef = maxStep / minStep;
            if (minStep == Math.Abs(YStep))
            {
                if(YStep > 0)
                {
                      YStep = 1;
                }
                else
                {
                    YStep = -1;
                }
                if (XStep >0 )
                {
                    XStep = koef;
                }
                else
                {
                   XStep = -koef;     
                }
            }
            else
            {
                if (XStep > 0)
                {
                    XStep = 1;
                }
                else
                {
                    XStep = -1;
                }
                if (YStep > 0)
                {
                    YStep = koef;
                }
                else
                {
                    YStep = -koef;
                }
            }
            int time = 0;
            while (Math.Abs(currX) < Math.Abs(coordinates[2]) && Math.Abs(CurrY) < Math.Abs(coordinates[3]))
            {
                currX += XStep;
                CurrY += YStep;
                moving += $"Curr position {Math.Round(currX, 2)}, {Math.Round(CurrY, 2)} time spent {time}; \n";
            }
            while (Math.Abs(currX) < Math.Abs(coordinates[2]))
            {
                currX += XStep / Math.Abs(XStep);
                moving += $"Curr position {Math.Round(currX, 2)}, {Math.Round(CurrY, 2)} time spent {time}; \n";
            }
            while (Math.Abs(CurrY) < Math.Abs(coordinates[3]))
            {
                CurrY += XStep / Math.Abs(YStep);
                moving += $"Curr position {Math.Round(currX, 2)}, {Math.Round(CurrY, 2)} time spent {time}; \n";
            }
            return moving;
        }
        int min(int par1, int par2)
        {
            return (par1 < par2)? par1 : par2;
        }
        int max(int par1, int par2)
        {
            return (par1 > par2) ? par1 : par2;
        }
    }
}
